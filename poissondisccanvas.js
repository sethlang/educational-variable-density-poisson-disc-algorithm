const jetBrainsMono = new FontFace("jetBrainsMono", "url(static/JetBrainsMono.ttf)");
document.fonts.add(jetBrainsMono);

const canvasSleepTime = 1000;
const canvasColors = {
  white: "white",
  black: "black",
  whiteTransparent: "rgba(255, 255, 255, 0.5)",
  whiteMoreTransparent: "rgba(255, 255, 255, 0.2)",
  whiteVeryTransparent: "rgba(255, 255, 255, 0.1)",
  blackTransparent: "rgba(0, 0, 0, 0.5)",
  blackVeryTransparent: "rgba(0, 0, 0, 0.15",
  green: "rgb(100, 150, 100)",
  greenAlt: "rgb(31, 46, 31)",
  greenTransparent: "rgba(100, 150, 100, 0.5)",
  greenVeryTransparent: "rgba(100, 150, 100, 0.1)",
  red: "rgb(170, 70, 70)",
  redTransparent: "rgba(170, 70, 70, 0.25)"
}

const samplingCanvas = document.getElementById("poissonDiscCanvas");
const samplingCanvasContext = poissonDiscCanvas.getContext("2d");

const pointSize = 6;
samplingCanvasContext.lineWidth = 2;

const gridSizeW = samplingCanvas.width;
const gridSizeH = samplingCanvas.height;
const minDist = gridSizeW / 8;
const cellSize = minDist / Math.sqrt(2);
const maxDist = Math.max(cellSize * 0.49, minDist * 3);

const columns = Math.floor(gridSizeW / cellSize);
const rows = Math.floor(gridSizeH / cellSize);
const gridCenterX = (columns * cellSize) / 2;
const gridCenterY = (rows * cellSize) / 2;

samplingCanvas.width = (columns * cellSize) + 1;
samplingCanvas.height = (rows * cellSize) + 1;

const attemptsToAddPoints = 3;

let activeCells = [];
let finalGrid = {};
let currentAttemptsRemaining = 0;
let currentActivePoint = null;
let currentVariableDist = null;
let attemptPoint = null;
let neighboringCellsChecked = [];
let allPosibleNeighbors = [];


function drawDistanceArcs(point) {
  const pointVariableDist = variableDistance(point);

  if (point == currentActivePoint) {
    samplingCanvasContext.setLineDash([10, 10]);
  } else {
    samplingCanvasContext.lineWidth = 4;
  }
  
  samplingCanvasContext.beginPath();
  samplingCanvasContext.arc(
    point.x, point.y,
    pointVariableDist, 0, Math.PI * 2
  );
  samplingCanvasContext.stroke();

  if (point == currentActivePoint) {
    samplingCanvasContext.beginPath();
    samplingCanvasContext.arc(
      point.x, point.y,
      pointVariableDist * 2, 0, Math.PI * 2
    );
    samplingCanvasContext.stroke();

    samplingCanvasContext.setLineDash([]);
  } else {
    samplingCanvasContext.lineWidth = 2;
  }
}

function drawAllNeighbors() {
  samplingCanvasContext.fillStyle = canvasColors.greenVeryTransparent;

  allPosibleNeighbors.forEach((cell) => {
    if (!neighboringCellsChecked.includes(cell)) {
      samplingCanvasContext.fillRect(
        (cell % columns) * cellSize,
        Math.floor(cell / columns) * cellSize,
        cellSize,
        cellSize
      );
    }
  });
}

function drawGrid() {
  samplingCanvasContext.strokeStyle = canvasColors.whiteMoreTransparent;
  samplingCanvasContext.fillStyle = canvasColors.redTransparent;
  
  for (let c = 0; c < columns; c++) {
    for (let r = 0; r < rows; r++) {
      const cell = c + (r * columns);
      
      if (neighboringCellsChecked.includes(cell)) {
        samplingCanvasContext.fillRect(
          c * cellSize,
          r * cellSize,
          cellSize,
          cellSize
        );
      }

      samplingCanvasContext.strokeRect(
        c * cellSize,
        r * cellSize,
        cellSize,
        cellSize
      );

      // cell numbers
      // samplingCanvasContext.fillStyle = canvasColors.whiteTransparent;
      // samplingCanvasContext.fillText(
      //   cell.toString(), c * cellSize, (r + 1) * cellSize
      // );
    }
  }
}

function drawSampling() {
  samplingCanvasContext.clearRect(0, 0, samplingCanvas.width, samplingCanvas.height);

  samplingCanvasContext.fillStyle = canvasColors.blackVeryTransparent;
  samplingCanvasContext.fillRect(0, 0, cellSize * columns, cellSize * rows);

  const currentAttemptsColorValue = 255 * (Math.min(1.0, 0.4 + (currentAttemptsRemaining / attemptsToAddPoints)));
  const currentAttemptsColor = "rgb(" + currentAttemptsColorValue.toString() + ", " + currentAttemptsColorValue.toString() + ", " + currentAttemptsColorValue.toString() + ")";


  // distance circles
  if (attemptPoint != null) {
    samplingCanvasContext.strokeStyle = canvasColors.green;
;
    drawDistanceArcs(attemptPoint);
  }
  if (currentActivePoint != null) {
    samplingCanvasContext.strokeStyle = currentAttemptsColor;
    drawDistanceArcs(currentActivePoint)
  }

  drawGrid();

  // grid points
  for (const [cell, point] of Object.entries(finalGrid)) {
    if (!activeCells.includes(cell)) {
      samplingCanvasContext.fillStyle = canvasColors.white;
      samplingCanvasContext.fillRect(
        point.x - (pointSize / 2) - 2, point.y - (pointSize / 2) - 2,
        pointSize + 4, pointSize + 4
      );

      samplingCanvasContext.fillStyle = canvasColors.black;
      samplingCanvasContext.fillRect(
        point.x - (pointSize / 2), point.y - (pointSize / 2),
        pointSize, pointSize
      );
    }
  }

  samplingCanvasContext.fillStyle = canvasColors.white;
  activeCells.forEach((cell) => {
    const point = finalGrid[cell.toString()];

    samplingCanvasContext.fillRect(
      point.x - (pointSize / 2), point.y - (pointSize / 2),
      pointSize, pointSize
    );
  });

  if (attemptPoint) {
    samplingCanvasContext.fillStyle = canvasColors.green;
    samplingCanvasContext.fillRect(
      attemptPoint.x - ((pointSize * 1.5) / 2.0), attemptPoint.y - ((pointSize * 1.5) / 2.0),
      pointSize * 1.5, pointSize * 1.5
    );
  }

  if (currentActivePoint) {
    samplingCanvasContext.fillStyle = currentAttemptsColor;
    samplingCanvasContext.fillRect(
      currentActivePoint.x - ((pointSize * 1.5) / 2.0), currentActivePoint.y - ((pointSize * 1.5) / 2.0),
      pointSize * 1.5, pointSize * 1.5
    );
  }
}

function drawNeighborDistanceLine(neighborPoint, tooClose) {
  const distancePath = new Path2D;
  distancePath.moveTo(attemptPoint.x, attemptPoint.y);
  distancePath.lineTo(neighborPoint.x, neighborPoint.y);

  if (tooClose) {
    samplingCanvasContext.strokeStyle = canvasColors.red;
  } else {
    samplingCanvasContext.strokeStyle = canvasColors.green;
  }

  samplingCanvasContext.lineWidth = 5;
  samplingCanvasContext.stroke(distancePath);
  samplingCanvasContext.lineWidth = 2;
}

function pointsEqual(a, b) {
  return a.x == b.x && a.y == b.y
}

function distanceBetweenPoints(a, b) {
  return Math.sqrt(((a.x - b.x) * (a.x - b.x)) + ((a.y - b.y) * (a.y - b.y)));
}

function cellFromPoint(point) {
  const column = Math.floor(point.x / cellSize)
  const row = Math.floor(point.y / cellSize)

  return column + (row * columns)
}

function randomPoint() {
  return {
    x: Math.random() * (columns * cellSize),
    y: Math.random() * (rows * cellSize)
  }
}

function randomDirection() {
  let randomDir = {x: Math.random(), y: Math.random()};

  while (randomDir.x < 0.1 && randomDir.y < 0.1) {
    randomDir = {x: Math.random(), y: Math.random()};
  }

  if (Math.random() > 0.5) {
    randomDir.x *= -1;
  }
  if (Math.random() > 0.5) {
    randomDir.y *= -1;
  }

  const norm = Math.sqrt((randomDir.x * randomDir.x) + (randomDir.y * randomDir.y));
  randomDir.x /= norm;
  randomDir.y /= norm;

  return randomDir
}

function variableDistance(point) {
  // const distToRight = Math.abs(point.x - (columns * cellSize));
  // const distToBot = Math.abs(point.y - (rows * cellSize));
  // const avgDistToTopRight = ((distToRight / gridCenterX) + (point.y / gridCenterY)) / 2;
  // const avgDistToBotLeft = ((point.x / gridCenterX) + (distToBot / gridCenterY)) / 2;
  // const minAvgDist = Math.min(avgDistToTopRight, avgDistToBotLeft);
  
  // const variableDist = Math.max(Math.min(minDist, maxDist), minDist + (minAvgDist * (maxDist - minDist)));
  
  const xDistToLeft = point.x / (columns * cellSize);
  const yDistToTop = point.y / (rows * cellSize);
  
  const averageDist = (xDistToLeft + yDistToTop) / 2;
  const variableDist = minDist + (averageDist * (maxDist - minDist));
  
  return variableDist
}

function randomAttemptPoint() {
  let randomPoint = null;
  let activeVariableDist = variableDistance(currentActivePoint);

  for (i = 0; i < 10; i++) {
    const randomDir = randomDirection();
    const randomDistance = activeVariableDist + (activeVariableDist * Math.random());

    const possiblePoint = {
      x: currentActivePoint.x + (randomDir.x * randomDistance),
      y: currentActivePoint.y + (randomDir.y * randomDistance)
    };

    if (possiblePoint.x >= 0 && possiblePoint.x <= columns * cellSize && possiblePoint.y >= 0 && possiblePoint.y <= rows * cellSize) {
      randomPoint = possiblePoint;
      break
    }
  }

  return randomPoint
}

function checkNeighbors() {
  const neighborPoint = finalGrid[neighboringCellsChecked.pop().toString()];

  let neighborDistance = (attemptPoint.x - neighborPoint.x) * (attemptPoint.x - neighborPoint.x);
  neighborDistance += (attemptPoint.y - neighborPoint.y) * (attemptPoint.y - neighborPoint.y);
  neighborDistance = Math.sqrt(neighborDistance);

  const tooClose = neighborDistance < currentVariableDist;


  drawSampling();
  drawNeighborDistanceLine(neighborPoint, tooClose);
  
  if (tooClose) {
    attemptPoint = null;
    neighboringCellsChecked = [];
  }

  setTimeout(state, canvasSleepTime);
}

function cellReachable(cellMinPoint) {
  for (let x = 0; x <= 1; x++) {
    for (let y = 0; y <= 1; y++) {
      const cornerPoint = {
        x: cellMinPoint.x + (x * cellSize),
        y: cellMinPoint.y + (y * cellSize)
      };

      if (distanceBetweenPoints(cornerPoint, attemptPoint) <= currentVariableDist) {
        return true
      }
    }
  }

  return false
}

function attemptNewPoint() {
  currentAttemptsRemaining -= 1;

  const randomNearbyPoint = randomAttemptPoint();

  if (randomNearbyPoint != null) {
    attemptPoint = randomNearbyPoint;
    currentVariableDist = variableDistance(attemptPoint);

    const cellDistance = Math.ceil(currentVariableDist / cellSize);
    const attemptCell = cellFromPoint(attemptPoint);
    const attemptPointCol = Math.floor(attemptPoint.x / cellSize);
    const attemptPointRow = Math.floor(attemptPoint.y / cellSize);
  
    neighboringCellsChecked = [];
    allPosibleNeighbors = [];
  
    for (let col = -cellDistance; col <= cellDistance; col++) {
      for (let row = -cellDistance; row <= cellDistance; row++) {
        if (Math.abs(col) == cellDistance && Math.abs(row) == cellDistance) {
          continue
        } else if (attemptPointCol + col < 0 || attemptPointCol + col >= columns) {
          continue
        } else if (attemptPointRow + row < 0 || attemptPointRow + row >= rows) {
          continue
        }
        
        const neighborCell = attemptCell + (col + (row * columns));
        const neighborCellMin = {
          x: (neighborCell % columns) * cellSize,
          y: Math.floor(neighborCell / columns) * cellSize
        }
        
        if (cellReachable(neighborCellMin)) {
          allPosibleNeighbors.push(neighborCell);

          if (neighborCell.toString() in finalGrid) {
            neighboringCellsChecked.push(neighborCell);
          }
        }
      }
    }
  
    drawSampling();
    drawAllNeighbors();
    setTimeout(state, canvasSleepTime);
  } else {
    attemptNewPoint();
  }
}

function iterateSampling() {
  const randomActiveIndex = Math.floor(Math.random() * activeCells.length);
  const randomActiveCell = activeCells.splice(randomActiveIndex, 1);
  currentActivePoint = finalGrid[randomActiveCell.toString()];

  currentAttemptsRemaining = attemptsToAddPoints;

  drawSampling();
  setTimeout(state, canvasSleepTime);
}

function newSampling() {
  const initialPoint = randomPoint();
  const initialPointCell = cellFromPoint(initialPoint);

  activeCells = [];
  finalGrid = {};
  currentActivePoint = null;
  currentAttemptsRemaining = 0;

  activeCells.push(initialPointCell);
  finalGrid[initialPointCell.toString()] = initialPoint;
  drawSampling();

  setTimeout(state, canvasSleepTime);
}

function state() {
  if (neighboringCellsChecked.length > 0) {
    checkNeighbors();

  } else if (attemptPoint != null && neighboringCellsChecked.length == 0) {
    const attemptCell = cellFromPoint(attemptPoint);
    activeCells.push(attemptCell);
    finalGrid[attemptCell.toString()] = attemptPoint;

    attemptPoint = null;
    neighboringCellsChecked = [];
    allPosibleNeighbors = [];

    drawSampling();
    setTimeout(state, canvasSleepTime);

  } else if (currentAttemptsRemaining > 0) {
    attemptNewPoint();

  } else if (activeCells.length > 0) {
    iterateSampling();

  } else {
    newSampling();
  }
}

newSampling();
